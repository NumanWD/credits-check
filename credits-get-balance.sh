#!/bin/bash

DIR="$(dirname "$0")"
. "$DIR/config.cfg"

COIN="CRDS"
CRDS_EXPLORER="https://explorer.crds.co"
API_KEY="d86ad5dee7719937"
CRDS_API="https://coinlib.io/api/v1/coin?key=${API_KEY}&pref=EUR&symbol=${COIN}"
CRDS_PRICE_EUR=$(curl --silent ${CRDS_API} | jq '.price' | bc)
CRDS_BALANCE_OLD=$(cat /tmp/crds_balance.txt 2> /dev/null || echo "${#MN[@]} * 5000" | bc)

TOTAL_M=0
CRDS_MN_ERROR=0

for i in "${MN[@]}"
do
  BALANCE=$(curl --silent "$CRDS_EXPLORER/ext/getbalance/$i")
  sleep 5
  TOTAL_M=$(echo "${TOTAL_M} + ${BALANCE}" | bc)
done

TOTAL=$TOTAL_M

for i in "${A[@]}"
do
  BALANCE=$(curl --silent "$CRDS_EXPLORER/ext/getbalance/$i")
  TOTAL=$(echo "${TOTAL} + ${BALANCE}" | bc)
done

TOTAL_F=$(echo "$TOTAL" | bc | xargs printf "%'.0f\n")
TOTAL_W=$(echo "${CRDS_PRICE_EUR} * ${TOTAL}" | bc | xargs printf "%'.0f\n")

# TODO Implement CHECK MN

if [ "$CRDS_MN_ERROR" -eq "0" ]; then
  FOOTER="All Masternodes are healthy [No check]"
else
  FOOTER="${CRDS_MN_ERROR} MN have an error"
fi

TITLE="[$COIN] Total $TOTAL_F - 🏦 $TOTAL_W €"

# Earn
MINED=$(echo "${TOTAL_M} - ${CRDS_BALANCE_OLD}" | bc | xargs printf "%'.2f")
SOME_EARN=$(echo $MINED'>'0 | bc -l)

if [ "$SOME_EARN" -gt "0" ]; then
  EARNED=$(echo "${CRDS_PRICE_EUR} * ${MINED}" | bc | xargs printf "%0.2f\n")
  /usr/local/bin/slackr -f "$FOOTER" -t "$TITLE" -c good "💰$MINED Earned - $EARNED €"
else
  /usr/local/bin/slackr -f "$FOOTER" -t "$TITLE" -c warning "💸 Nothing today"
fi

echo $TOTAL_M > /tmp/crds_balance.txt