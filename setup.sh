#!/bin/bash

CYAN='\033[0;36m'
GREEN='\033[0;92m'
BLUE='\033[0;94m'
RED='\033[0;31m'
NC='\033[0m' # No Color

TYPE_CHECK='check'
TYPE_ALL='all'
TYPE_BALANCE='balance'

pwd=$(pwd)


while getopts ":t:" opt; do
  case $opt in
    t) TYPE="$OPTARG"
    ;;
    \?) echo -e "${RED}Incorrect Type (-t).${NC}"
        echo -e "${GREEN}Allow options: ${TYPE_CHECK}, ${TYPE_BALANCE}, ${TYPE_ALL}${NC}"
        exit 1
    ;;
  esac
done


if  [ ! -z "${TYPE}" ]
then
  case "${TYPE}" in
    ${TYPE_CHECK} | ${TYPE_ALL} | ${TYPE_BALANCE} )
      ;;
    *)
      echo -e "${RED}Incorrect Type (-t).${NC}"
      echo -e "${GREEN}Allow options: ${TYPE_CHECK}, ${TYPE_BALANCE}, ${TYPE_ALL}${NC}"
      exit 1
  esac
fi

# Install Dependencies
if [ ! -x "$(command -v bc)" ] || [ ! -x "$(command -v jq)" ]; then
  echo -e "\n${GREEN}Installing Dependencies${NC}"
  sudo apt-get update
  sudo apt -y install bc jq
fi


# Install Slackr
if ! [ -x "$(command -v slackr)" ]; then
  echo -e "\n${GREEN}Installing Slackr${NC}"
  curl -sL https://github.com/a-sync/slackr/archive/master.tar.gz | tar xz
  cd slackr-master && chmod +x slackr
  echo "Create your WEBHOOK https://my.slack.com/services/new/incoming-webhook/"
  read -p "Enter webhook URL: " WHURL && sed -i "3s|.*|WEBHOOK_URL=\"$WHURL\"|" slackr
  sudo ln -s "$(readlink -f slackr)" /usr/local/bin
  cd ..
fi


#Install CronJob Balance
install_cron_balance=$(crontab -l | grep credits-get-balance.sh | wc -l)

if [ "$install_cron_balance" -eq "0" ] && [ "${TYPE}" = "${TYPE_BALANCE}" ]; then
  echo -e "\n${GREEN}Installing Cron Balance${NC}"
  #Create Config file
  read -p "Enter Masternodo Address:" ADDRESS && echo "MN=($ADDRESS)" > config.cfg

  chmod +x credits-get-balance.sh
  ## Add cronjob
  (crontab -l 2>/dev/null; echo "40 23 * * * /bin/bash $pwd/credits-get-balance.sh /dev/null 2>&1 ") | crontab -
fi


#Install CronJob Checker
install_cron_checker=$(crontab -l | grep credits-checker.sh | wc -l)

if [ "$install_cron_checker" -eq "0" ] && [ "${TYPE}" = "${TYPE_CHECK}" ]; then
  echo -e "\n${GREEN}Installing Cron Check${NC}"
  chmod +x credits-checker.sh
  ## Add cronjob
  (crontab -l 2>/dev/null; echo "*/2 * * * * /bin/bash $pwd/credits-checker.sh /dev/null 2>&1 ") | crontab -
fi