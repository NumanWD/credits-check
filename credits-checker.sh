#!/bin/bash

COIN="CRDS"
DAEMON="credits"
RESTART="$HOME/credits/bin/creditsd -daemon"
CLI="$HOME/credits/bin/credits-cli"

lines=$(pgrep $DAEMON | wc -l)

if [ ! "$lines" -gt "0" ]; then
  $RESTART
  /usr/local/bin/slackr -c danger \[$COIN\] Restart $(hostname)
  exit 1
fi

status=$($CLI masternode status | jq '.status')

if [ ! $status -eq 4 ]; then
  /usr/local/bin/slackr -c danger \[$COIN\] Masternode Down $(hostname)
  exit 1
fi